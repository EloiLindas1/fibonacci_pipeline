import sys
module_path = "../"
if module_path not in sys.path:
    sys.path.append(module_path)

from fibonacci import fibonacci

def test_fibonacci_first_sequence():
    assert fibonacci(1) == 1

def test_fibonacci_ten():
    assert fibonacci(10) == 55

def test_fibonacci_negative():
    pass

